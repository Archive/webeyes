/*
 * WebEyes: A Simple web search applet
 *
 *	(c) Copyright 2000-2002 Alan Cox, All Rights Reserved
 *	(c) Copyright 2002 Robert McQueen, All Rights Reserved
 *	(c) Copyright 2002 Philip Downer. All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 * Author : Matt Keenan <Matt.Keenan@sun.com>
 */

#ifdef HAVE_CONFIG_H
#	include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <dirent.h>
#include <sys/stat.h>
#include <time.h>

#include <gnome.h>
#include <libbonoboui.h>
#include <glade/glade.h>
#include <gconf/gconf.h>
#include <panel-applet.h>
#include <panel-applet-gconf.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include <libgnome/gnome-program.h>
#include <libgnome/gnome-help.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-url.h>
#include <libgnomeui/gnome-window-icon.h>
#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-ui-init.h>
#include <libgnomeui/gnome-client.h>
#include <libgnomevfs/gnome-vfs-utils.h>

#include "webeyes.h"

/*
 * Function prototypes
 */
static void webeyes_init_icons();
static void register_webeyes_stock_icons(GtkIconFactory *factory);
static void delete_history_entry(int element_number);
static int exists_history_entry(int pos);
static char *get_history_entry(int pos);
static void append_history_entry(const char *entry);

static void about_cb(BonoboUIComponent *uic,
				gpointer user_data,
				const gchar *verbname);
static void help_cb(BonoboUIComponent *uic,
				gpointer user_data,
				const gchar *verbname);
static void preferences_cb(BonoboUIComponent *uic,
				gpointer user_data,
				const gchar *verbname);
static void preferences_response_cb(GtkDialog *dialog,
				gint response,
				GladeXML *glade);
static void preferences_spin_value_changed_cb(GtkSpinButton *spinbutton,
				GladeXML *glade);
static void preferences_radio_toggled_cb(GtkWidget *radiobutton,
				GladeXML *glade);
static gboolean webeyes_button_pressed_cb(GtkButton *button,
				GdkEventButton *event,
				gpointer data);
static gboolean webeyes_button_clicked_cb(GtkButton *button,
				GdkEventButton *event,
				gpointer data);
static void launch_search(char *search_criteria);
static void webeyes_window_selection_received(GtkWidget *widget,
			   	GtkSelectionData *data,
				gpointer val,
				char *url);
static gboolean webeyes_session_die (GtkWidget *widget, gpointer data);
static gboolean webeyes_applet_layout (WebEyesApplet *data);
static gboolean gnome_webeyes_applet_fill(PanelApplet *parent_applet);
static gboolean gnome_webeyes_applet_factory(PanelApplet *applet,
				const gchar		*iid,
				gpointer		data);
static gboolean webeyes_size_allocate_cb(GtkWidget *w,
				GtkAllocation *allocation,
				WebEyesApplet *data);
static gboolean webeyes_button_cross_cb(GtkWidget *w,
				GdkEventCrossing *event,
				WebEyesApplet *data);
static gboolean webeyes_button_focus_cb(GtkWidget *w,
				GdkEventFocus *event,
				WebEyesApplet *data);
static void webeyes_update_button_images(WebEyesApplet *data, gboolean highlight, gboolean button_pressed);
static void webeyes_load_images(WebEyesApplet *data, int icon_size);

static void webeyes_cmd_completion(char *cmd);
static gchar *history_auto_complete(GtkWidget *widget,
				GdkEventKey *event);
static void set_atk_name(GtkWidget *widget,
				const char *name);

static gboolean entry_event(GtkEntry *entry, GdkEventKey *event, gpointer data);
static gboolean entry_activate(GtkEntry *entry, GdkEventKey *event, gpointer data);
static gboolean entry_changed(GtkEntry *entry, GdkEventKey *event, gpointer data);


/*
 * Globals
 */
static const BonoboUIVerb gnome_webeyes_menu_verbs [] = {
		BONOBO_UI_VERB ("prefs", preferences_cb),
		BONOBO_UI_VERB ("help", help_cb),
		BONOBO_UI_VERB ("about", about_cb),
		BONOBO_UI_VERB_END
};
static WebEyesApplet *webeyes_applet;
static char *history_entry[WEBEYES_HISTORY_LIST_LENGTH];
static gint history_position = WEBEYES_HISTORY_LIST_LENGTH;
static GtkIconSize button_icon_size = 0;
static time_t last_search_time;
static time_t text_entry_time;
static char current_command[WEBEYES_MAX_COMMAND_LENGTH];

static WebEyesStockIcon  webeyes_icons[] = {
	{ WEBEYES_STOCK_DEFAULT_ICON, GNOME_ICONDIR"/webeyes.png" },
	{ WEBEYES_STOCK_HISTORY_ICON, GNOME_ICONDIR"/webeyes-history.png" },
	{ WEBEYES_STOCK_DEFAULT_PRELIGHT_ICON, GNOME_ICONDIR"/webeyes-prelight.png" },
	{ WEBEYES_STOCK_HISTORY_PRELIGHT_ICON, GNOME_ICONDIR"/webeyes-history-prelight.png" }
};

static gboolean icons_initialized = FALSE;
static GtkIconSize webeyes_icon_size = 0;

static void
delete_history_entry(int element_number)
{
	int pos;

	for(pos = element_number; pos > 0; --pos)
		history_entry[pos] = history_entry[pos - 1];

	history_entry[0] = NULL;
}

static gchar *
history_auto_complete(GtkWidget *widget,
					GdkEventKey *event)
{
	gchar current_command[WEBEYES_MAX_COMMAND_LENGTH];
	gchar* completed_command;
	int i;

	g_snprintf(current_command, sizeof(current_command), "%s",
			gtk_entry_get_text(GTK_ENTRY(widget)));

	if (!strlen(current_command))
		return NULL;

	for(i = WEBEYES_HISTORY_LIST_LENGTH - 1; i >= 0; i--) {
		if(!exists_history_entry(i))
			break;
		completed_command = get_history_entry(i);

		if (!strncmp(completed_command, current_command, strlen(current_command)))
			return completed_command;
	}
	return NULL;
}

static int
exists_history_entry(int pos)
{
	return(history_entry[pos] != NULL);
}

static char *
get_history_entry(int pos)
{
	return(history_entry[pos]);
}

static void
append_history_entry(const char *entry)
{
	int pos, i;
	GConfValue *history;
	GSList *list = NULL;
	GList *items = NULL;
	
	/* remove older dupes */
	for(pos = 0; pos <= WEBEYES_HISTORY_LIST_LENGTH - 1; pos++) {
		if(exists_history_entry(pos) && strcmp(entry, history_entry[pos]) == 0)
			delete_history_entry(pos); /* dupe found */
	}

	/* delete oldest entry */
	if (history_entry[0] != NULL)
		g_free(history_entry[0]);

	/* move entries */
	for(pos = 0; pos < WEBEYES_HISTORY_LIST_LENGTH - 1; pos++) {
		history_entry[pos] = history_entry[pos+1];
	}

	/* append entry */
	history_entry[WEBEYES_HISTORY_LIST_LENGTH-1] =
			(char *)g_malloc(sizeof(char) * (strlen(entry)+1));

	strcpy(history_entry[WEBEYES_HISTORY_LIST_LENGTH-1], entry);

	/* Append the search to the history */
	gnome_entry_prepend_history(GNOME_ENTRY(webeyes_applet->combo), FALSE, entry);

	/* Store the history into gconf */
	for (i=0; i<WEBEYES_HISTORY_LIST_LENGTH; i++) {
		GConfValue *entry;

		entry = gconf_value_new(GCONF_VALUE_STRING);
		if (exists_history_entry(i)) {
			gconf_value_set_string(entry, (gchar *) get_history_entry(i));
			list = g_slist_append(list, entry);
		}
	}

	history = gconf_value_new(GCONF_VALUE_LIST);
	if (list) {
		gconf_value_set_list_type(history, GCONF_VALUE_STRING);
		gconf_value_set_list(history, list);
		panel_applet_gconf_set_value(webeyes_applet->applet, "history", history, NULL);
	}

	while (list) {
		GConfValue *value = list->data;
		gconf_value_free(value);
		list = g_slist_next(list);
	}
	gconf_value_free(history);
}

static void
set_atk_name(GtkWidget *widget, const char *name)
{
	AtkObject *aobj;

	aobj = gtk_widget_get_accessible(widget);
	if (GTK_IS_ACCESSIBLE(aobj) == FALSE || !name)
		return;

	atk_object_set_name(aobj, name);
}

static void
preferences_spin_value_changed_cb(GtkSpinButton *spinbutton,
				GladeXML *glade)
{
	GtkAdjustment *applet_size_spin;
	gint applet_size;

	applet_size_spin = gtk_spin_button_get_adjustment(
					GTK_SPIN_BUTTON(glade_xml_get_widget(glade, "applet_size")));
	applet_size = gtk_adjustment_get_value(applet_size_spin);

	if (applet_size != webeyes_applet->applet_size && applet_size > 0) {
		gtk_widget_set_size_request(
					GTK_WIDGET(webeyes_applet->entry), applet_size, -1);
		webeyes_applet->applet_size = applet_size;
		panel_applet_gconf_set_int(webeyes_applet->applet, "applet_size", 
						applet_size, NULL);
	}
}

static void
preferences_radio_toggled_cb(GtkWidget *radiobutton,
				GladeXML *glade)
{
	GSList *radio_group;
	GtkWidget *browser_default = glade_xml_get_widget(glade, "browser_default");
	GtkWidget *browser_custom = glade_xml_get_widget(glade, "browser_custom");
	GtkWidget *browser_entry = glade_xml_get_widget(glade, "browser_entry");
	gboolean browser_default_state;

	browser_default_state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(browser_default));

	if (browser_default_state != webeyes_applet->browser_default) {
		if (browser_default_state == TRUE) {
			webeyes_applet->browser_default = TRUE;
			webeyes_applet->browser_custom = FALSE;
			gtk_editable_set_editable(GTK_EDITABLE(browser_entry), FALSE);
			gtk_widget_set_sensitive(GTK_WIDGET(browser_entry), FALSE);
		}
		else {
			webeyes_applet->browser_default = FALSE;
			webeyes_applet->browser_custom = TRUE;
			gtk_editable_set_editable(GTK_EDITABLE(browser_entry), TRUE);
			gtk_widget_set_sensitive(GTK_WIDGET(browser_entry), TRUE);
		}
	}
}

static void
preferences_response_cb(GtkDialog *dialog,
						gint response,
						GladeXML *glade)
{
	GError *error = NULL;
	char *prefstr;
	char *custombrowserstr;
	gint applet_size;
	gboolean enable_history;
	GtkAdjustment *applet_size_spin;
	gboolean browser_default_state;

	switch (response) {
		case GTK_RESPONSE_HELP:
			gnome_help_display("webeyes.xml", NULL, &error);
			if (error) {
				GtkWidget *message_dialog;

				message_dialog = gtk_message_dialog_new(GTK_WINDOW(dialog),
										GTK_DIALOG_DESTROY_WITH_PARENT,
										GTK_MESSAGE_ERROR,
										GTK_BUTTONS_CLOSE,
										_("There was an error displaying help:\n%s"),
										error->message);
				g_error_free(error);
				g_signal_connect(G_OBJECT(message_dialog), "response",
								G_CALLBACK(gtk_widget_destroy), NULL);
				gtk_window_set_resizable(GTK_WINDOW(message_dialog), FALSE);
				gtk_widget_show(message_dialog);
			}
			break;
		case GTK_RESPONSE_CLOSE:
			prefstr = g_strdup(gtk_entry_get_text(
					GTK_ENTRY(glade_xml_get_widget(glade, "url_entry"))));

			if (prefstr != NULL) {
				webeyes_applet->url = g_strdup(prefstr);
				panel_applet_gconf_set_string(webeyes_applet->applet, "url", 
								prefstr, NULL);
				g_free(prefstr);
			}

			applet_size_spin = gtk_spin_button_get_adjustment(
							GTK_SPIN_BUTTON(glade_xml_get_widget(glade, "applet_size")));
			applet_size = gtk_adjustment_get_value(applet_size_spin);
			enable_history = gtk_toggle_button_get_active(
					GTK_TOGGLE_BUTTON(glade_xml_get_widget(glade, "enable_history")));
			browser_default_state = gtk_toggle_button_get_active(
					GTK_TOGGLE_BUTTON(glade_xml_get_widget(glade, "browser_default")));

			if (applet_size != webeyes_applet->applet_size) {
				/* resize applet */
				webeyes_applet->applet_size = applet_size;
				gtk_widget_set_size_request(GTK_WIDGET(webeyes_applet->entry),
								webeyes_applet->applet_size, -1);
				panel_applet_gconf_set_int(webeyes_applet->applet, "applet_size", 
								applet_size, NULL);
			}

			if (enable_history != webeyes_applet->enable_history) {
				/* Enable/Disable History */
				webeyes_applet->enable_history = enable_history;
				panel_applet_gconf_set_bool(webeyes_applet->applet, "enable_history", 
								enable_history, NULL);
			}

			if (browser_default_state != webeyes_applet->browser_default) {
				if (browser_default_state == TRUE) {
					webeyes_applet->browser_default = TRUE;
					webeyes_applet->browser_custom = FALSE;

					panel_applet_gconf_set_bool(webeyes_applet->applet,
									"browser_default", TRUE, NULL);
					panel_applet_gconf_set_bool(webeyes_applet->applet,
									"browser_custom", FALSE, NULL);
				}
				else {
					webeyes_applet->browser_default = FALSE;
					webeyes_applet->browser_custom = TRUE;
					panel_applet_gconf_set_bool(webeyes_applet->applet,
									"browser_default", FALSE, NULL);
					panel_applet_gconf_set_bool(webeyes_applet->applet,
									"browser_custom", TRUE, NULL);
				}
			}

			/* Set the custom browser string if custom is being used */
			if (webeyes_applet->browser_custom == TRUE) {
				prefstr = g_strdup(gtk_entry_get_text(
					GTK_ENTRY(glade_xml_get_widget(glade, "browser_entry"))));

				if (prefstr != NULL) {
					webeyes_applet->browser_command = g_strdup(prefstr);
					panel_applet_gconf_set_string(webeyes_applet->applet,
									"url", prefstr, NULL);
					g_free(prefstr);
				}
			}

			gtk_widget_destroy(GTK_WIDGET(dialog));
			g_object_unref(glade);
			break;
		default:
			break;
	}
}

static void
set_tooltip (GtkWidget	*widget,
             const char *tip)
{
 	GtkTooltips *tooltips;

	tooltips = g_object_get_data (G_OBJECT (widget), "tooltips");
	if (!tooltips) {
		tooltips = gtk_tooltips_new ();
		g_object_ref (tooltips);
		gtk_object_sink (GTK_OBJECT (tooltips));
		g_object_set_data_full (
			G_OBJECT (widget), "tooltips", tooltips,
			(GDestroyNotify) g_object_unref);
	}

        gtk_tooltips_set_tip (tooltips, widget, tip, NULL);
}

static void
help_cb(BonoboUIComponent *uic,
		gpointer user_data,
		const gchar *verbname)
{
	GError *error = NULL;

	gnome_help_display("webeyes.xml", NULL, &error);
	if (error) {
		g_warning( "Error opening help :\n%s\n", error->message);
		g_error_free(error);
	}
}

static void
preferences_cb(BonoboUIComponent *uic,
				gpointer user_data,
				const gchar *verbname)
{
	GladeXML *glade = glade_xml_new(GLADE_PATH, "preferences_dialog", NULL);
	GtkWidget *dialog = glade_xml_get_widget(glade, "preferences_dialog");
	GtkAdjustment *applet_size_spin = gtk_spin_button_get_adjustment(
					GTK_SPIN_BUTTON(glade_xml_get_widget(glade, "applet_size")));
	GSList *radio_group = gtk_radio_button_get_group(
					GTK_RADIO_BUTTON(glade_xml_get_widget(glade, "browser_default")));
	GtkWidget *browser_default = glade_xml_get_widget(glade, "browser_default");
	GtkWidget *browser_custom = glade_xml_get_widget(glade, "browser_custom");
	GtkWidget *browser_entry = glade_xml_get_widget(glade, "browser_entry");

	g_signal_connect(G_OBJECT(dialog), "response",
			G_CALLBACK(preferences_response_cb), glade);

	g_signal_connect(G_OBJECT(applet_size_spin), "value_changed",
					G_CALLBACK(preferences_spin_value_changed_cb), glade);

	g_signal_connect(G_OBJECT(browser_default), "toggled",
					G_CALLBACK(preferences_radio_toggled_cb), glade);
	g_signal_connect(G_OBJECT(browser_custom), "toggled",
					G_CALLBACK(preferences_radio_toggled_cb), glade);

	gtk_entry_set_text(
			GTK_ENTRY(glade_xml_get_widget(glade, "url_entry")),
			webeyes_applet->url);

	gtk_adjustment_set_value(applet_size_spin, webeyes_applet->applet_size);

	gtk_toggle_button_set_active(
			GTK_TOGGLE_BUTTON(glade_xml_get_widget(glade, "enable_history")),
			webeyes_applet->enable_history);

	gtk_entry_set_text(GTK_ENTRY(browser_entry), webeyes_applet->browser_command);

	if (webeyes_applet->browser_default == TRUE) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(browser_default), TRUE);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(browser_custom), FALSE);
		gtk_editable_set_editable(GTK_EDITABLE(browser_entry), FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(browser_entry), FALSE);
	}
	else {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(browser_default), FALSE);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(browser_custom), TRUE);
		gtk_editable_set_editable(GTK_EDITABLE(browser_entry), TRUE);
		gtk_widget_set_sensitive(GTK_WIDGET(browser_entry), TRUE);
	}

	gtk_widget_show(dialog);
}

static void
about_cb(BonoboUIComponent *uic,
		gpointer user_data,
		const gchar *verbname)
{
	GladeXML *glade = glade_xml_new(GLADE_PATH, "about_dialog", NULL);
	GtkWidget *dialog = glade_xml_get_widget(glade, "about_dialog");

	/* FIXME : fix because libglade does not properly set these */
	g_object_set(G_OBJECT(dialog), "name", _("Internet Search"),
					"version", VERSION);

	gtk_widget_show(dialog);
	g_object_unref(glade);
}

/* do not eat button press! */
static gboolean
webeyes_button_pressed_cb(GtkButton *button,
						GdkEventButton *event,
						gpointer data)
{
	WebEyesApplet *applet_data = (WebEyesApplet *) data;

	if (event->button == 3 || event->button == 2) {
		gtk_propagate_event(GTK_WIDGET(applet_data->applet), (GdkEvent *) event);
		return TRUE;
	}
	return FALSE;
}

/* do not eat button press! */
static gboolean
webeyes_button_clicked_cb(GtkButton *button,
						GdkEventButton *event,
						gpointer data)
{
	WebEyesApplet *applet_data = (WebEyesApplet *) data;

	panel_applet_request_focus (applet_data->applet, event->time);

	if (event->button != 1)
		g_signal_stop_emission_by_name (button, "button_press_event");
	else if (event->type == GDK_BUTTON_PRESS) {
		webeyes_update_button_images(applet_data, TRUE, TRUE);
	}
	else if (event->type == GDK_BUTTON_RELEASE) {
		static GdkAtom selection_atom = GDK_NONE;
		GtkWidget *window = (GtkWidget *)applet_data->applet;

		webeyes_update_button_images(applet_data, TRUE, FALSE);

		if (selection_atom==GDK_NONE)
			selection_atom=gdk_atom_intern("STRING", FALSE);

		if (!gtk_selection_convert(window, GDK_SELECTION_PRIMARY,
			selection_atom, GDK_CURRENT_TIME))
		{
			g_warning (_("Could not get primary selection\n"));
		}
	}

	return FALSE;
}

static gboolean
entry_changed(GtkEntry *entry, GdkEventKey *event, gpointer data)
{
	text_entry_time = time(NULL);
}

static gboolean
entry_activate(GtkEntry *entry, GdkEventKey *event, gpointer data)
{
	int propagate_event = TRUE;
	char *command;
	char buffer[WEBEYES_MAX_COMMAND_LENGTH];

	command = (char *)g_malloc(sizeof(char) * WEBEYES_MAX_COMMAND_LENGTH+1);
	strcpy(command, (char *)gtk_entry_get_text(GTK_ENTRY(webeyes_applet->entry)));
	if (command != NULL) {
		if (strlen(command) > 0 ) {
			launch_search(command);
			gtk_entry_set_text(GTK_ENTRY(webeyes_applet->entry), (gchar *) command);
			append_history_entry(command);
			history_position = WEBEYES_HISTORY_LIST_LENGTH;
		}
		g_free(command);
		strcpy(current_command, "");
		gtk_editable_select_region(GTK_EDITABLE(webeyes_applet->entry), 0, 0);
		propagate_event = FALSE;
	}

	return !propagate_event;
}

static gboolean
entry_event(GtkEntry *entry, GdkEventKey *event, gpointer data)
{
	guint key = event->keyval;
	char *command;
	char buffer[WEBEYES_MAX_COMMAND_LENGTH];
	gboolean propagate_event = TRUE;
	gchar *preedit_string = NULL;
	gint cursor_pos;

        if (event->type == GDK_BUTTON_PRESS)
		panel_applet_request_focus (webeyes_applet->applet, event->time);

	if (event->type != GDK_KEY_PRESS)
		return FALSE;

	if (entry->editable) {
		gtk_im_context_get_preedit_string(entry->im_context, &preedit_string, NULL, &cursor_pos);
		if (strlen(preedit_string)) {
			g_free (preedit_string);
			return !(propagate_event = TRUE);
		}
		g_free (preedit_string);
	}

	if (key == GDK_Tab || 
		key == GDK_KP_Tab ||
		key == GDK_ISO_Left_Tab) {
		if (event->state == GDK_CONTROL_MASK) {
			gtk_widget_child_focus(GTK_WIDGET(webeyes_applet->applet),
							GTK_DIR_TAB_FORWARD);
			propagate_event = FALSE;
		}
		else if (event->state != GDK_SHIFT_MASK) {
			strcpy(buffer, (char *)gtk_entry_get_text(GTK_ENTRY(webeyes_applet->entry)));
			webeyes_cmd_completion(buffer);
			gtk_entry_set_text(GTK_ENTRY(webeyes_applet->entry), (gchar *)buffer);
			propagate_event = FALSE;
		}
	}
	else if (key == GDK_Up ||
			key == GDK_KP_Up ||
			key == GDK_ISO_Move_Line_Up ||
			key == GDK_Pointer_Up) {
		if (history_position == WEBEYES_HISTORY_LIST_LENGTH) {
			strcpy(current_command, (char *) gtk_entry_get_text(GTK_ENTRY(webeyes_applet->entry)));
		}

		if (history_position > 0 && exists_history_entry(history_position -1)) {
			gtk_entry_set_text(GTK_ENTRY(webeyes_applet->entry), (gchar *)
								get_history_entry(--history_position));
		}

		propagate_event = FALSE;
	}
	else if (key == GDK_Down ||
			key == GDK_KP_Down ||
			key == GDK_ISO_Move_Line_Down ||
			key == GDK_Pointer_Down) {
		if (history_position < WEBEYES_HISTORY_LIST_LENGTH -1) {
			gtk_entry_set_text(GTK_ENTRY(webeyes_applet->entry),
							(gchar *)get_history_entry(++history_position));
		}
		else if (history_position == WEBEYES_HISTORY_LIST_LENGTH-1) {
			gtk_entry_set_text(GTK_ENTRY(webeyes_applet->entry),
							(gchar *) current_command);
			++history_position;
		}
		propagate_event = FALSE;
	}
	/*
	else if (key == GDK_Return ||
			key == GDK_KP_Enter ||
			key == GDK_ISO_Enter ||
			key == GDK_3270_Enter) {
		command = (char *)g_malloc(sizeof(char) * WEBEYES_MAX_COMMAND_LENGTH+1);
		strcpy(command, (char *)gtk_entry_get_text(GTK_ENTRY(webeyes_applet->entry)));
		if (command != NULL) {
			if (strlen(command) > 0 ) {
				launch_search(command);
				gtk_entry_set_text(GTK_ENTRY(webeyes_applet->entry), (gchar *) command);
				append_history_entry(command);
				history_position = WEBEYES_HISTORY_LIST_LENGTH;
			}
			g_free(command);
			strcpy(current_command, "");
			gtk_editable_select_region(GTK_EDITABLE(webeyes_applet->entry), 0, 0);
			propagate_event = TRUE;
		}
	}
	*/
	else if (key == GDK_space && event->state == GDK_CONTROL_MASK) {
		propagate_event = TRUE;
	}
	else if (webeyes_applet->enable_history &&
			key >= GDK_space &&
			key <= GDK_asciitilde ) {
		char *completed_command = NULL;
		gint current_position = gtk_editable_get_position(
											GTK_EDITABLE(webeyes_applet->entry));
		if (current_position >= 0) {
			gtk_editable_delete_selection(GTK_EDITABLE(webeyes_applet->entry));
			current_position = gtk_editable_get_position(
												GTK_EDITABLE(webeyes_applet->entry));
			completed_command = history_auto_complete(
									GTK_WIDGET(webeyes_applet->entry), event);

			if (completed_command != NULL) {
				gtk_entry_set_text(GTK_ENTRY(webeyes_applet->entry),
									completed_command);
				gtk_editable_set_position(GTK_EDITABLE(webeyes_applet->entry),
											current_position);
				gtk_editable_select_region(GTK_EDITABLE(webeyes_applet->entry),
											current_position, -1);
				propagate_event = FALSE;
			}
		}
	}
	return !propagate_event;
}

static void
launch_search(gchar *search_criteria)
{
	gchar *p, *stp, *tp, *bptr;
	gchar *st1, *st2;
	gchar *url, *urlorig;
	gunichar c;

	last_search_time = time(NULL);
	
	/* A UTF-8 char has 6 bytes and they are converted to URI;
	 * %xx%xx%xx. i.e, the buffer needs 9 * (len) and not 3 */
	bptr=g_malloc(9*strlen(search_criteria)+strlen(webeyes_applet->url)+1);
	if(bptr==NULL)
		return;
	p=search_criteria;
	/*tp=bptr+sprintf(bptr, webeyes_applet->url);*/
	stp=tp=g_malloc(9 * sizeof(char) * (strlen(search_criteria)+1));
	while(*p)
	{
		c = g_utf8_get_char (p);
		if (g_unichar_isspace (c))
		{
			const gchar *plus = "+";
			g_utf8_strncpy (tp, plus, 1);
			tp ++;
			p = g_utf8_next_char (p);
		}
		else
		{
			gchar up[7];
			gchar *escaped = NULL;
			int usize = g_unichar_to_utf8 (c, up);
			int escaped_len;

			up[usize] = '\0';
			escaped = gnome_vfs_escape_string (up);
			escaped_len = strlen (escaped);
			strncpy (tp, escaped, escaped_len);
			tp += escaped_len;
			g_free (escaped);
			p = g_utf8_next_char (p);
		}
	}
	*tp=0;

	url = urlorig = g_strdup(webeyes_applet->url);

	if ((st1=strtok(url, WEBEYES_SEARCH_TEXT_KEYWORD)) != NULL) {
		st2=strtok(NULL, WEBEYES_SEARCH_TEXT_KEYWORD);
		if (st2 == NULL) {
			sprintf(bptr, "%s%s\0", st1, stp);
		}
		else {
			sprintf(bptr, "%s%s%s\0", st1, stp, st2);
		}
	}
	else {
		g_warning(_("Search engine URL missing %s substitution keyword\n"), 
						WEBEYES_SEARCH_TEXT_KEYWORD);
		g_free(stp);
		g_free(bptr);
		return;
	}
	g_free(stp);
	g_free(urlorig);

	if (bptr && strlen(bptr) > 0) {
		if (webeyes_applet->browser_default == TRUE) {
#if (GTK_MAJOR_VERSION > 1)	
			{
				GError *err = NULL;
			
				gnome_url_show(bptr, &err);
				if (err != NULL) { 
					g_warning (bptr);
		    		g_error_free (err);
	        	}
			}
#else
			gnome_url_show(bptr);
#endif
		}
		else {
			/* Use custom defined browser command provided in preferences */
			GError *err = NULL;
			gboolean ret;
			char *str;
			int i;
			char **argv;
			int argc;

			str = g_malloc(sizeof(char) * (strlen(webeyes_applet->browser_command) +
											strlen(bptr) + 1));
			sprintf(str, webeyes_applet->browser_command, bptr);

			if (!g_shell_parse_argv(str, &argc, &argv, &err)) {
				if (err != NULL) {
					g_warning(str);
					g_error_free(err);
				}
				g_free(str);
				g_free(bptr);
				return;
			}

			err = NULL;
			ret = g_spawn_async(NULL, /* working directory */
								argv, /* arguments */
								NULL, /* environment (use parent) */
								G_SPAWN_SEARCH_PATH, /* flags */
								NULL, /* child setup */
								NULL, /* data */
								NULL, /* child PID */
								&err);
			g_strfreev(argv);

			if (err != NULL) {
				g_warning(str);
				g_error_free(err);
			}
			g_free(str);
		}
	}
	if (bptr)
		g_free(bptr);
}

static void
webeyes_window_selection_received(GtkWidget *widget,
			   	GtkSelectionData *data,
				gpointer val,
				char *url)
{
	char *data_ptr = NULL, *search_ptr = NULL;
	int i = 0, j = 0;
	static char *prev_search_ptr = NULL;

	if(data->length<0 || data->type!=GDK_SELECTION_TYPE_STRING) {
		search_ptr = g_strdup((char *)gtk_entry_get_text(GTK_ENTRY(webeyes_applet->entry)));
		if (!search_ptr || strlen(search_ptr)==0)
			return;
	}
	else {
		data_ptr = g_malloc(data->length+2);
		if (data_ptr == NULL)
			return;

		/* Copy only printable characters */
		search_ptr = data_ptr;
		for (i=0, j=0; i<=data->length && data->data[i]; i++) {
			if (data->data[i] > 31 && data->data[i] < 127)
				data_ptr[j++] = data->data[i];
		}
		data_ptr[j] = '\0';
	
		if (!prev_search_ptr) {
			prev_search_ptr = g_strdup(search_ptr);
		}
		else {
			if (strcmp(prev_search_ptr, search_ptr)) {
				/* Previous Selection is different from current selection
			 	* so launch search with current selection */
				g_free(prev_search_ptr);
				prev_search_ptr = g_strdup(search_ptr);
			}
			else {
				/* Previous Selection is the same as the current so
			 	* check timestamp of last search, if Entry has changed
			 	* since last search then use entry as search string */
	
				if (text_entry_time > last_search_time) {
			 		/* text was edited since last search, so use text contents
				 	* for searching */
			 		g_free(search_ptr);
					search_ptr = g_strdup((char *)gtk_entry_get_text(GTK_ENTRY(webeyes_applet->entry)));
			 	}
			}
		}
	}

	launch_search(search_ptr);
	append_history_entry(search_ptr);
	history_position = WEBEYES_HISTORY_LIST_LENGTH;
	gtk_entry_set_text(GTK_ENTRY(webeyes_applet->entry), (gchar *) search_ptr);
	g_free(search_ptr);
}

static void
webeyes_cmd_completion(char *cmd)
{
	char buffer[WEBEYES_MAX_COMMAND_LENGTH] = "";
	char largest_possible_completion[WEBEYES_MAX_COMMAND_LENGTH] = "";
	int i, pos;
	GList *possible_completion_list = NULL;
	GList *completion_element;

	if (strlen(cmd) == 0)
		return;

	completion_element = g_list_first(possible_completion_list);
	if (completion_element)
		strcpy(largest_possible_completion, (char *)completion_element->data);
	else
		strcpy(largest_possible_completion, "");

	while ((completion_element = g_list_next(completion_element))) {
		strcpy(buffer, (char *) completion_element->data);
		pos = 0;

		while (largest_possible_completion[pos] != '\000' &&
				buffer[pos] != '\000' &&
				strncmp(largest_possible_completion, buffer, pos+1) == 0)
			pos++;

		strncpy(largest_possible_completion, buffer, pos);
		largest_possible_completion[pos] = '\000';
	}

	if (strlen(largest_possible_completion) > 0) {
		strcpy(cmd, largest_possible_completion);
	}
}

static gboolean
webeyes_session_die (GtkWidget *widget, gpointer data)
{
	gtk_main_quit();
	return TRUE;
}

static void
webeyes_update_button_images(WebEyesApplet *data, gboolean highlight, gboolean button_pressed)
{
 	gint size;
	GdkPixbuf *scaled1 = NULL;
	GdkPixbuf *scaled2 = NULL;
	GtkRequisition button_req, image_req;
	int width, height;
	
	gtk_widget_size_request (GTK_WIDGET(data->button), &button_req);
	gtk_widget_size_request (GTK_BIN(data->button)->child, &image_req);
	width = data->panel_size - (button_req.width - image_req.width);
	height = data->panel_size - (button_req.height - image_req.height);
	size = MIN (width, height);

	if (highlight) {
		scaled1 = gdk_pixbuf_scale_simple (data->webeyes_prelight_image, size, size,
                                          	GDK_INTERP_BILINEAR);
	}
	else {
		scaled1 = gdk_pixbuf_scale_simple (data->webeyes_image, size, size,
                                          	GDK_INTERP_BILINEAR);
	}
	scaled2 = gdk_pixbuf_copy(scaled1);
	if (button_pressed)
		gdk_pixbuf_scale(scaled1, scaled2, 0, 0, size, size, 1, 1, 1, 1, GDK_INTERP_BILINEAR);
	gtk_image_set_from_pixbuf (GTK_IMAGE (data->scaled_image), scaled2);
	g_object_unref (scaled1);
	g_object_unref (scaled2);
}

static gboolean
webeyes_change_orient_cb(GtkWidget *w, PanelAppletOrient orient, WebEyesApplet *data)
{
        gtk_container_remove (GTK_CONTAINER(data->box), data->event_box);
        gtk_container_remove (GTK_CONTAINER(data->box), data->history_event_box);
	gtk_widget_destroy (data->box);
	data->panel_orient = orient;
	webeyes_applet_layout (data);
	webeyes_update_button_images(data, FALSE, FALSE);
}

static gboolean
webeyes_change_size_cb(GtkWidget *w, int size, WebEyesApplet *applet)
{
	webeyes_load_images (applet, size);
}

static gboolean
webeyes_size_allocate_cb(GtkWidget *w,
			 GtkAllocation *allocation,
			 WebEyesApplet *data)
{
        if (data->panel_orient == PANEL_APPLET_ORIENT_DOWN || data->panel_orient == PANEL_APPLET_ORIENT_UP) {
	        if (data->panel_size == allocation->height)
		        return TRUE;
		data->panel_size = allocation->height;
	} else {
	        if (data->panel_size == allocation->width)
		        return TRUE;
		data->panel_size = allocation->width;
	}
	gtk_container_remove (GTK_CONTAINER(data->box), data->event_box);
	gtk_container_remove (GTK_CONTAINER(data->box), data->history_event_box);
	gtk_widget_destroy (data->box);
        webeyes_applet_layout (data);
	webeyes_update_button_images(data, FALSE, FALSE);
	
	return TRUE;
}

static gboolean
webeyes_button_cross_cb(GtkWidget *w,
				GdkEventCrossing *event,
				WebEyesApplet *data)
{
	if (event->type == GDK_ENTER_NOTIFY || GTK_WIDGET_HAS_FOCUS(data->button))
		webeyes_update_button_images(data, TRUE, FALSE);
	else
		webeyes_update_button_images(data, FALSE, FALSE);

	return TRUE;
}

static gboolean
webeyes_button_focus_cb(GtkWidget *w,
				GdkEventFocus *event,
				WebEyesApplet *data)
{
	if (event->in)
		webeyes_update_button_images(data, TRUE, FALSE);
	else
		webeyes_update_button_images(data, FALSE, FALSE);

	return TRUE;
}

static void
register_webeyes_stock_icons(GtkIconFactory *factory)
{
	GtkIconSource *source;
	int i;

	source = gtk_icon_source_new();

	for (i=0; i<G_N_ELEMENTS(webeyes_icons); i++) {
		GtkIconSet *icon_set;
		gtk_icon_source_set_filename(source, webeyes_icons[i].icon_data);
		icon_set = gtk_icon_set_new();
		gtk_icon_set_add_source(icon_set, source);
		gtk_icon_factory_add(factory, webeyes_icons[i].stock_id, icon_set);
		gtk_icon_set_unref(icon_set);
	}
	gtk_icon_source_free(source);
}

static void
webeyes_init_icons()
{
	GtkIconFactory *factory;

	if (icons_initialized)
		return;

	webeyes_icon_size = gtk_icon_size_register ("panel-menu",
						    WEBEYES_DEFAULT_ICON_SIZE,
						    WEBEYES_DEFAULT_ICON_SIZE);

	factory = gtk_icon_factory_new ();
	gtk_icon_factory_add_default (factory);
	
	register_webeyes_stock_icons(factory);

	g_object_unref(factory);

	icons_initialized = TRUE;
}

/* colorshift pixbuf */
static void
webeyes_make_prelight_icon(GdkPixbuf *dest, GdkPixbuf *src, int shift)
{
       gint i, j;
       gint width, height, has_alpha, srcrowstride, destrowstride;
       guchar *target_pixels;
       guchar *original_pixels;
       guchar *pixsrc;
       guchar *pixdest;
       int val;
       guchar r,g,b;
                                                                                
       has_alpha = gdk_pixbuf_get_has_alpha (src);
       width = gdk_pixbuf_get_width (src);
       height = gdk_pixbuf_get_height (src);
       srcrowstride = gdk_pixbuf_get_rowstride (src);
       destrowstride = gdk_pixbuf_get_rowstride (dest);
       target_pixels = gdk_pixbuf_get_pixels (dest);
       original_pixels = gdk_pixbuf_get_pixels (src);
                                                                                
       for (i = 0; i < height; i++) {
               pixdest = target_pixels + i*destrowstride;
               pixsrc = original_pixels + i*srcrowstride;
               for (j = 0; j < width; j++) {
                       r = *(pixsrc++);
                       g = *(pixsrc++);
                       b = *(pixsrc++);
                       val = r + shift;
                       *(pixdest++) = CLAMP(val, 0, 255);
                       val = g + shift;
                       *(pixdest++) = CLAMP(val, 0, 255);
                       val = b + shift;
                       *(pixdest++) = CLAMP(val, 0, 255);
                       if (has_alpha)
                               *(pixdest++) = *(pixsrc++);
               }
       }
}

static void
webeyes_load_images(WebEyesApplet *data, int stock_icon_size)
{
    WebEyesApplet *webeyes_data = (WebEyesApplet *)data;
    GtkIconTheme *icon_theme;
    GtkIconInfo *webeyes_icon;
    GError *error = NULL;

    if (webeyes_data->webeyes_image) {
	    g_object_unref (webeyes_data->webeyes_image);
	    g_object_unref (webeyes_data->webeyes_prelight_image);
    }

    icon_theme = gtk_icon_theme_get_default ();
    webeyes_icon = gtk_icon_theme_lookup_icon (icon_theme,
					       "gnome-searchtool",
					       stock_icon_size, 0);

    if (webeyes_icon) {
    	webeyes_data->webeyes_image = gtk_icon_info_load_icon (webeyes_icon, &error);
        if (error) {
		g_warning (G_STRLOC ": cannot open %s: %s", gtk_icon_info_get_filename (webeyes_icon), error->message);
		g_error_free (error);

    		webeyes_data->webeyes_image =
				gtk_widget_render_icon(GTK_WIDGET(webeyes_data->applet),
                                                  WEBEYES_STOCK_DEFAULT_ICON,
                                                  webeyes_icon_size, NULL);
	}
    }
    else
    	webeyes_data->webeyes_image =
				gtk_widget_render_icon(GTK_WIDGET(webeyes_data->applet),
                                                  WEBEYES_STOCK_DEFAULT_ICON,
                                                  webeyes_icon_size, NULL);
    webeyes_data->webeyes_prelight_image =
				gdk_pixbuf_new(gdk_pixbuf_get_colorspace(webeyes_data->webeyes_image),
							gdk_pixbuf_get_has_alpha (webeyes_data->webeyes_image),
							gdk_pixbuf_get_bits_per_sample(webeyes_data->webeyes_image),
							gdk_pixbuf_get_width(webeyes_data->webeyes_image),
							gdk_pixbuf_get_height(webeyes_data->webeyes_image));
    webeyes_make_prelight_icon(webeyes_data->webeyes_prelight_image, webeyes_data->webeyes_image, 30);
}

static gboolean
webeyes_applet_layout(WebEyesApplet *data)
{
        if ((data->panel_orient == PANEL_APPLET_ORIENT_UP || data->panel_orient == PANEL_APPLET_ORIENT_DOWN) &&
	    (data->panel_size <= 48) )
	        webeyes_applet->box = gtk_hbox_new(FALSE, 0);
	else
	        webeyes_applet->box = gtk_vbox_new(FALSE, 0);

        set_atk_name(GTK_WIDGET(webeyes_applet->box), _("WebEyes Box"));

	gtk_container_add(GTK_CONTAINER(webeyes_applet->box),
					webeyes_applet->event_box);
	gtk_container_add(GTK_CONTAINER(webeyes_applet->box),
					webeyes_applet->history_event_box);

	gtk_container_add(GTK_CONTAINER(webeyes_applet->applet), webeyes_applet->box);
	gtk_widget_show_all(webeyes_applet->box);
}

static inline void
force_no_focus_padding (GtkWidget *widget)
{
        gboolean first_time = TRUE;

        if (first_time) {
                gtk_rc_parse_string ("\n"
                                     "   style \"webeyes-applet-button-style\"\n"
                                     "   {\n"
                                     "      GtkWidget::focus-line-width=0\n"
                                     "      GtkWidget::focus-padding=0\n"
                                     "   }\n"
                                     "\n"
                                     "    widget \"*.webeyes-applet-button\" style \"webeyes-applet-button-style\"\n"
                                     "\n");
                first_time = FALSE;
        }

        gtk_widget_set_name (widget, "webeyes-applet-button");
}

static gboolean
gnome_webeyes_applet_fill(PanelApplet *parent_applet)
{
	GtkTooltips *webeyes_tooltips;
	gint default_width;
	GConfValue *history;

	gtk_selection_clear_targets(GTK_WIDGET(parent_applet), GDK_SELECTION_PRIMARY);

	webeyes_init_icons();

	webeyes_applet = g_new0 (WebEyesApplet, 1);
	webeyes_applet->applet = parent_applet;
	set_atk_name(GTK_WIDGET(webeyes_applet->applet), _("WebEyes"));

	panel_applet_add_preferences(webeyes_applet->applet,
					"/schemas/apps/webeyes/prefs", NULL);
	panel_applet_set_flags (parent_applet, PANEL_APPLET_EXPAND_MINOR);
	webeyes_applet->panel_orient = panel_applet_get_orient (parent_applet);
	webeyes_applet->panel_size = panel_applet_get_size (parent_applet);
	webeyes_applet->url =
			g_strdup(panel_applet_gconf_get_string(webeyes_applet->applet, "url",
						NULL));
	webeyes_applet->applet_size =
			panel_applet_gconf_get_int(webeyes_applet->applet, "applet_size", NULL);
	webeyes_applet->enable_history = TRUE;

	history = panel_applet_gconf_get_value(webeyes_applet->applet, "history", NULL);
	if (history) {
		GSList *list;

		for (list=gconf_value_get_list(history); list; list = list->next) {
			const char *entry = NULL;

			if ((entry = gconf_value_get_string(list->data)))
				append_history_entry(entry);
		}
		gconf_value_free(history);
	}

	webeyes_applet->browser_default = TRUE;
	webeyes_applet->browser_custom = FALSE;
	webeyes_applet->browser_command =
			g_strdup(panel_applet_gconf_get_string(webeyes_applet->applet,
					"browser_command", NULL));

	webeyes_load_images(webeyes_applet, webeyes_applet->panel_size);

	gtk_window_set_default_icon_name ("gnome-searchtool");
	webeyes_tooltips = gtk_tooltips_new();

	webeyes_applet->event_box =  gtk_event_box_new();
	set_atk_name(GTK_WIDGET(webeyes_applet->event_box), _("WebEyes Search Event Box"));
	set_tooltip(webeyes_applet->event_box, _("WebEyes"));

	webeyes_applet->button = gtk_button_new();
	set_atk_name(GTK_WIDGET(webeyes_applet->button), _("WebEyes Search"));
	set_tooltip(webeyes_applet->button, _("WebEyes"));
	force_no_focus_padding (webeyes_applet->button);

	webeyes_applet->scaled_image = gtk_image_new ();
	set_atk_name(GTK_WIDGET(webeyes_applet->scaled_image), _("WebEyes Search Image"));
	gtk_container_add(GTK_CONTAINER(webeyes_applet->button),
					webeyes_applet->scaled_image);
	gtk_container_add(GTK_CONTAINER(webeyes_applet->event_box),
					webeyes_applet->button);

	/* History GtkCombo */
	webeyes_applet->history_event_box =  gtk_event_box_new();
	set_atk_name(GTK_WIDGET(webeyes_applet->history_event_box), _("WebEyes History Event Box"));
	set_tooltip(webeyes_applet->history_event_box, _("WebEyes History"));

	webeyes_applet->combo = gnome_entry_new(NULL);
	set_tooltip(webeyes_applet->combo, _("WebEyes History"));
	webeyes_applet->entry = gnome_entry_gtk_entry(GNOME_ENTRY(webeyes_applet->combo));
	set_tooltip(webeyes_applet->entry, _("WebEyes History"));
	webeyes_applet->list = GTK_COMBO(webeyes_applet->combo)->list;
	gtk_combo_set_use_arrows(GTK_COMBO(webeyes_applet->combo), TRUE);
	gtk_combo_set_use_arrows_always(GTK_COMBO(webeyes_applet->combo), TRUE);
	gnome_entry_set_max_saved(GNOME_ENTRY(webeyes_applet->combo),
							WEBEYES_HISTORY_LIST_LENGTH);

	gtk_container_add(GTK_CONTAINER(webeyes_applet->history_event_box),
					webeyes_applet->combo);

	gtk_widget_set_size_request(GTK_WIDGET(webeyes_applet->entry),
								webeyes_applet->applet_size, -1);
	
	/* We ref the two widgets so they are not destroyed when reassigned to a new box in webeyes_applet_layout */
	g_object_ref (webeyes_applet->event_box);
	g_object_ref (webeyes_applet->history_event_box);
 
	webeyes_applet_layout(webeyes_applet);

	webeyes_update_button_images(webeyes_applet, FALSE, FALSE);

	panel_applet_setup_menu_from_file(PANEL_APPLET(webeyes_applet->applet),
					NULL, "GNOME_WebEyes.xml",
					NULL, gnome_webeyes_menu_verbs, webeyes_applet);

	gtk_widget_show(GTK_WIDGET(webeyes_applet->applet));

	gtk_entry_set_text(GTK_ENTRY(webeyes_applet->entry), "");

	g_signal_connect(G_OBJECT(webeyes_applet->button), "button_press_event",
					G_CALLBACK(webeyes_button_clicked_cb),
					(gpointer)webeyes_applet);

	g_signal_connect(G_OBJECT(webeyes_applet->button), "button_release_event",
					G_CALLBACK(webeyes_button_clicked_cb),
					(gpointer)webeyes_applet);


	g_signal_connect(G_OBJECT(webeyes_applet->entry), "event_after",
					G_CALLBACK(entry_event), NULL);

	g_signal_connect(G_OBJECT(webeyes_applet->entry), "activate",
					G_CALLBACK(entry_activate), NULL);

	g_signal_connect(G_OBJECT(webeyes_applet->entry), "changed",
					G_CALLBACK(entry_changed), NULL);

  	g_signal_connect(G_OBJECT(webeyes_applet->applet), "destroy",
					G_CALLBACK(webeyes_session_die), NULL);
	g_signal_connect(G_OBJECT(webeyes_applet->applet), "selection-received",
					G_CALLBACK(webeyes_window_selection_received),
					webeyes_applet->url);
	g_signal_connect(G_OBJECT(webeyes_applet->applet), "change-size",
			                G_CALLBACK(webeyes_change_size_cb),
                      			 webeyes_applet);
	g_signal_connect(G_OBJECT(webeyes_applet->applet), "size-allocate",
					G_CALLBACK(webeyes_size_allocate_cb),
                                        webeyes_applet);
	g_signal_connect(G_OBJECT(webeyes_applet->applet), "change-orient",
				      G_CALLBACK(webeyes_change_orient_cb),
				      webeyes_applet);
	g_signal_connect(G_OBJECT(webeyes_applet->button), "enter-notify-event",
					G_CALLBACK(webeyes_button_cross_cb),
                                       webeyes_applet);
	g_signal_connect(G_OBJECT(webeyes_applet->button), "leave-notify-event",
					G_CALLBACK(webeyes_button_cross_cb),
                                       webeyes_applet);
	g_signal_connect(G_OBJECT(webeyes_applet->button), "focus-in-event",
					G_CALLBACK(webeyes_button_focus_cb),
                                       webeyes_applet);
	g_signal_connect(G_OBJECT(webeyes_applet->button), "focus-out-event",
					G_CALLBACK(webeyes_button_focus_cb),
                                       webeyes_applet);
	return TRUE;
}

static gboolean
gnome_webeyes_applet_factory(PanelApplet *applet,
								const gchar		*iid,
								gpointer		data)
{
	gboolean retval = FALSE;

	if (!strcmp(iid, "OAFIID:GNOME_WebEyes"))
		retval = gnome_webeyes_applet_fill(applet);

	return retval;
}

PANEL_APPLET_BONOBO_FACTORY ("OAFIID:GNOME_WebEyes_Factory",
				PANEL_TYPE_APPLET,
						"webeyes",
						"0",
						gnome_webeyes_applet_factory,
						NULL);
