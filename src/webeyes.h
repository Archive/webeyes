#ifndef __WEBEYES_H_
#define __WEBEYES_H_

/*
 * Matt Keenan  <matt.keenan@sun.com>
 *
 * WebEyes Panel Applet
 *
 */
 

#include <panel-applet.h>
#include <regex.h>
#include <glib/gmacros.h>

/*
 * WebEyes Applet Definitions
 */
#define GLADE_PATH							GNOMEGLADEDIR "/GNOME_WebEyes.glade"
#define WEBEYES_DEFAULT_ICON_SIZE			48 
#define WEBEYES_STOCK_DEFAULT_ICON			"webeyes-default-icon"
#define WEBEYES_STOCK_HISTORY_ICON 			"webeyes-history-icon"
#define WEBEYES_STOCK_DEFAULT_PRELIGHT_ICON	"webeyes-default-prelight-icon"
#define WEBEYES_STOCK_HISTORY_PRELIGHT_ICON	"webeyes-history-prelight-icon"
#define WEBEYES_HISTORY_LIST_LENGTH			50
#define WEBEYES_MAX_COMMAND_LENGTH			500
#define WEBEYES_DEFAULT_HISTORY_SIZE		6
#define WEBEYES_SEARCH_TEXT_KEYWORD			"WEBEYESTEXT"

typedef struct _WebEyesApplet {
	PanelApplet *applet;
	GtkWidget *button;
	GtkWidget *combo;
	GtkWidget *entry;
	GtkWidget *list;
	GtkWidget *scaled_image;
	GtkWidget *event_box;
	GtkWidget *history_event_box;
	GtkWidget *box;
	GtkWidget *vbox;

	GdkPixbuf *webeyes_image;
	GdkPixbuf *webeyes_prelight_image;

	char *url;
	gint applet_size;
        gint panel_size;
        PanelAppletOrient panel_orient;
	gboolean enable_history;
	gboolean browser_default;
	gboolean browser_custom;
	char *browser_command;
} WebEyesApplet;

typedef struct _WebEyesStockIcon {
	char *stock_id;
	const guint8 *icon_data;
} WebEyesStockIcon;

#endif
