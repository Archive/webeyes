# English (British) translation.
# Copyright (C) 2004 THE webeyes' COPYRIGHT HOLDER
# This file is distributed under the same license as the webeyes package.
# Gareth Owen <gowen72@yahoo.com>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: webeyes\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-09-07 21:09+0100\n"
"PO-Revision-Date: 2004-06-25 22:39-0400\n"
"Last-Translator: Gareth Owen <gowen72@yahoo.comg>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/GNOME_WebEyes.glade.h:1
msgid "*"
msgstr "*"

#: src/GNOME_WebEyes.glade.h:2
msgid "About WebEyes Applet"
msgstr "About WebEyes Applet"

#: src/GNOME_WebEyes.glade.h:3
msgid "Copyright 2003 Sun Microsystems"
msgstr "Copyright 2003 Sun Microsystems"

#: src/GNOME_WebEyes.glade.h:4
msgid "Preferences"
msgstr "Preferences"

#: src/GNOME_WebEyes.glade.h:5
msgid "Search _URL:"
msgstr "Search _URL:"

#: src/GNOME_WebEyes.glade.h:6
msgid "Update interval:"
msgstr "Update interval:"

#: src/GNOME_WebEyes.glade.h:7
msgid "Use _custom browser"
msgstr "Use _custom browser"

#: src/GNOME_WebEyes.glade.h:8
msgid "WebEyes Applet For The GNOME Desktop Environment"
msgstr "WebEyes Applet For The GNOME Desktop Environment"

#: src/GNOME_WebEyes.glade.h:9
msgid "WebEyes Preferences Dialog"
msgstr "WebEyes Preferences Dialogue"

#: src/GNOME_WebEyes.glade.h:10
msgid "_Autocomplete from previous searches"
msgstr "_Autocomplete from previous searches"

#: src/GNOME_WebEyes.glade.h:11
msgid "_Use default browser"
msgstr "_Use default browser"

#: src/GNOME_WebEyes.glade.h:12
msgid "_Width:"
msgstr "_Width:"

#: src/GNOME_WebEyes.glade.h:13
msgid "pixels"
msgstr "pixels"

#: src/GNOME_WebEyes.glade.h:14
msgid "translator_credits"
msgstr "Gareth Owen <gowen72@yahoo.com>"

#: src/GNOME_WebEyes.xml.h:1
msgid "_About..."
msgstr "_About..."

#: src/GNOME_WebEyes.xml.h:2
msgid "_Help"
msgstr "_Help"

#: src/GNOME_WebEyes.xml.h:3
msgid "_Preferences"
msgstr "_Preferences"

#: src/GNOME_WebEyes_Factory.server.in.in.h:1
msgid "Internet"
msgstr "Internet"

#. FIXME : fix because libglade does not properly set these
#: src/GNOME_WebEyes_Factory.server.in.in.h:2 src/webeyes.c:518
msgid "Internet Search"
msgstr "Internet Search"

#: src/GNOME_WebEyes_Factory.server.in.in.h:3
msgid "Search the internet for keywords"
msgstr "Search the internet for keywords"

#: src/GNOME_WebEyes_Factory.server.in.in.h:4
msgid "WebEyes Panel Applet Factory"
msgstr "WebEyes Panel Applet Factory"

#: src/WebEyes.schemas.in.h:1
msgid "Applet entry width"
msgstr "Applet entry width"

#: src/WebEyes.schemas.in.h:2
msgid "Browser Command"
msgstr "Browser Command"

#: src/WebEyes.schemas.in.h:3
msgid "Custom Browser"
msgstr "Custom Browser"

#: src/WebEyes.schemas.in.h:4
msgid "Default Browser"
msgstr "Default Browser"

#: src/WebEyes.schemas.in.h:5
msgid "Enable Autocompletion"
msgstr "Enable Autocompletion"

#: src/WebEyes.schemas.in.h:6
msgid "Enables autocompletion of typed search text."
msgstr "Enables autocompletion of typed search text."

#: src/WebEyes.schemas.in.h:7
msgid "Entry History"
msgstr "Entry History"

#: src/WebEyes.schemas.in.h:8
msgid "Entry search text history."
msgstr "Entry search text history."

#: src/WebEyes.schemas.in.h:9
msgid "Search URL"
msgstr "Search URL"

#: src/WebEyes.schemas.in.h:10
msgid "Specify a browser command to use."
msgstr "Specify a browser command to use."

#: src/WebEyes.schemas.in.h:11
msgid "The search engine URL to use."
msgstr "The search engine URL to use."

#: src/WebEyes.schemas.in.h:12
msgid "The width in characters for the search text entry field."
msgstr "The width in characters for the search text entry field."

#: src/WebEyes.schemas.in.h:13
msgid "Use a custom specified browser command."
msgstr "Use a custom specified browser command."

#: src/WebEyes.schemas.in.h:14
msgid "Use the default browser as setup in desktop preferences."
msgstr "Use the default browser as setup in desktop preferences."

#: src/webeyes.c:337
#, c-format
msgid ""
"There was an error displaying help:\n"
"%s"
msgstr ""
"There was an error displaying help:\n"
"%s"

#: src/webeyes.c:565
msgid "Could not get primary selection\n"
msgstr "Could not get primary selection\n"

#: src/webeyes.c:751
#, c-format
msgid "Search engine URL missing %s substitution keyword\n"
msgstr "Search engine URL missing %s substitution keyword\n"

#
#: src/webeyes.c:1149
msgid "WebEyes Box"
msgstr "WebEyes Box"

#: src/webeyes.c:1176 src/webeyes.c:1227 src/webeyes.c:1231
msgid "WebEyes"
msgstr "WebEyes"

#: src/webeyes.c:1226
msgid "WebEyes Search Event Box"
msgstr "WebEyes Search Event Box"

#: src/webeyes.c:1230
msgid "WebEyes Search"
msgstr "WebEyes Search"

#: src/webeyes.c:1234
msgid "WebEyes Search Image"
msgstr "WebEyes Search Image"

#: src/webeyes.c:1242
msgid "WebEyes History Event Box"
msgstr "WebEyes History Event Box"

#: src/webeyes.c:1243 src/webeyes.c:1246 src/webeyes.c:1248
msgid "WebEyes History"
msgstr "WebEyes History"

#~ msgid "Applet _width:"
#~ msgstr "Applet _width:"

#~ msgid "Webeyes quick search"
#~ msgstr "Webeyes quick search"
