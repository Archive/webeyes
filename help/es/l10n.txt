WebEyes Applet Manual V2.0.1

------------------------
SUMMARY OF CHANGES
------------------------

Created by: Breda McColgan
Date: 07 August 2003
Manual version: 2.0

Updated by: Breda McColgan
Date: 28 August 2003
Manual version: 2.0.1
Summary of changes:
* Updated for Mercury



------------------------
SCREENSHOT INSTRUCTIONS
------------------------


webeyes_applet.png

No localization required.
